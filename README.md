## Final Project 2
## Kelompok 15
## Anggota Kelompok
- Andika Dwiki Darmawan
- Annisa Yuli Setyaningsih
- Muhammad Ismail

## Tema Project
Book Store dengan konsep mini e-commerce

## ERD
![alt text](ERD.png)

## Link Video
- https://drive.google.com/file/d/1eDsoe2EMA5aiHlDze7ciYhBEbmZ2FHZs/view?usp=sharing

## Catatan :

- link dokumentasi POSTMAN : https://documenter.getpostman.com/view/18405650/UVR5q8gf
